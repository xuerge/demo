package com.xuerge.lab.cache;

import com.xuerge.lab.cache.bean.User;
import org.junit.Test;

import javax.cache.Cache;
import javax.cache.CacheManager;
import javax.cache.Caching;
import javax.cache.configuration.MutableConfiguration;
import javax.cache.spi.CachingProvider;

public class CsCache107Test {
    @Test
    public void test(){
        CachingProvider cachingProvider = Caching.getCachingProvider();
        CacheManager manager = cachingProvider.getCacheManager();
        Cache<String, User> cache = manager.createCache("Test", new MutableConfiguration<String, User>());
        String key = "leo";
        User user = new User();
        user.setName("leo");
        cache.put(key,user);
        System.out.println("Hello " + cache.get(key).getName());
    }
}
