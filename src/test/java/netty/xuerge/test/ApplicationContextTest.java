package netty.xuerge.test;

import com.xuerge.lab.domain.Class;
import com.xuerge.lab.domain.Student;

import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * @author shiyi.xu
 * @create 17/4/30.
 */
public class ApplicationContextTest {

    @Test
    public void get(){
        ApplicationContext container = new ClassPathXmlApplicationContext("applicationContext.xml");
        Student studentA = (Student) container.getBean("studentA");
        Class classA = (Class) container.getBean("classA");
        System.out.println(classA.getSt());
    }
}
