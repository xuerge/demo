package com.xuerge.lab.guava.cache;

import org.junit.Assert;
import org.junit.Before;

public class TestSim {
    GuavaCachDemoSim cachDemo = new GuavaCachDemoSim();
    @Before
    public void init(){
        cachDemo.InitLoadingCache();
    }
    @org.junit.Test
    public void getIfPresentloadingCache(){
        Assert.assertNull(cachDemo.getIfPresentloadingCache("as"));
    }

    @org.junit.Test
    public void getCacheKeyloadingCache(){
        Assert.assertNull(cachDemo.getCacheKeyloadingCache("as"));
    }

    @org.junit.Test
    public void putloadingCache(){
        cachDemo.putloadingCache("as",new Man());
        Assert.assertNotNull(cachDemo.getIfPresentloadingCache("as"));
    }


    public static void main(String[] args) {
        GuavaCachDemoSim cachDemo = new GuavaCachDemoSim();
        System.out.println("使用loadingCache");
        cachDemo.InitLoadingCache();

        System.out.println("使用loadingCache get方法  第一次加载");
        Man man = cachDemo.getCacheKeyloadingCache("001");
        System.out.println(man);

        System.out.println("\n使用loadingCache getIfPresent方法  第一次加载");
        man = cachDemo.getIfPresentloadingCache("002");
        System.out.println(man);

        System.out.println("\n使用loadingCache get方法  第一次加载");
        man = cachDemo.getCacheKeyloadingCache("002");
        System.out.println(man);

        System.out.println("\n使用loadingCache get方法  已加载过");
        man = cachDemo.getCacheKeyloadingCache("002");
        System.out.println(man);

        System.out.println("\n使用loadingCache get方法  已加载过,但是已经被剔除掉,验证重新加载");
        man = cachDemo.getCacheKeyloadingCache("001");
        System.out.println(man);

        System.out.println("\n使用loadingCache getIfPresent方法  已加载过");
        man = cachDemo.getIfPresentloadingCache("001");
        System.out.println(man);

        System.out.println("\n使用loadingCache put方法  再次get");
        Man newMan = new Man();
        newMan.setId("001");
        newMan.setName("额外添加");
        cachDemo.putloadingCache("001", newMan);
        man = cachDemo.getCacheKeyloadingCache("001");
        System.out.println(man);
    }
}
