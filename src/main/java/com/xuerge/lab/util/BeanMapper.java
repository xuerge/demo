package com.xuerge.lab.util;

import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

/**
 * @author shiyi.xu
 * @create 18/5/15.
 */
@Component
public class BeanMapper {

    public static <T> T map(Object valueObj, Class<T> clazz) {
        T t = null;
        try {
            t = clazz.newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (null != valueObj)
            BeanUtils.copyProperties(valueObj, t);
        return t;
    }
}
