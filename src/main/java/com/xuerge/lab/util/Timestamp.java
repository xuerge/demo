package com.xuerge.lab.util;

import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 * @author shiyi.xu
 * @create 17/6/22.
 */
public class Timestamp {
    public static String nextDay() {
        final GregorianCalendar calendar = new GregorianCalendar();
        calendar.add(GregorianCalendar.DATE, 1);
        calendar.set(Calendar.HOUR_OF_DAY, 10);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return String.valueOf(calendar.getTime().getTime());
    }

    public static String now() {
        return String.valueOf(System.currentTimeMillis());
    }
}
