package com.xuerge.lab.util;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.apache.log4j.Logger;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;

public class DateFormatUtils {

    private static final Logger logger = Logger.getLogger(DateFormatUtils.class);

    public static final String[] DATE_PATTERN = {"yyyy-MM-dd HH:mm:ss", "yyyy-MM-dd HH:mm",
            "yyyy-MM-dd HH", "yyyy-MM-dd", "yyyy-MM-dd HH:mm:ss.SSS", "yyyyMMddHHmmss", "yyyyMMdd"};

    /**
     * 验证Collection是否为空
     * 
     * @param c
     * @return
     */
    public static <T> Boolean isEmpty(Collection<T> c) {
        return c == null || c.isEmpty();
    }

    /**
     * 判断是否为空
     * 
     * @param str
     * @return
     */
    public static boolean isEmpty(String str) {
        return null == str || str.trim().length() == 0;
    }

    /**
     * 字符串转时间
     * 
     * @param dateStr
     * @return
     */
    public static Date strToDate(String dateStr) {
        if (StringUtils.isEmpty(dateStr)) {
            return null;
        }
        try {
            return DateUtils.parseDate(dateStr, DATE_PATTERN);
        } catch (ParseException e) {
            logger.error("时间转换失败", e);
        }
        return null;
    }

    /**
     * 日期格式化
     * 
     * @param date 需要格式化的参数
     * @param formatType 格式化类型 0："yyyy-MM-dd HH:mm:ss", 1："yyyy-MM-dd HH:mm", 2："yyyy-MM-dd HH",
     *        3："yyyy-MM-dd", 4："yyyy-MM-dd HH:mm:ss.SSS", 5："yyyyMMddHHmmss", 6："yyyyMMdd"
     * @return
     */
    public static String formatDate(Date date, Integer formatType) {
        if (date == null) {
            return "";
        }
        if (formatType == null) {
            formatType = 1;
        }
        SimpleDateFormat sdf = new SimpleDateFormat(DATE_PATTERN[formatType]);
        return sdf.format(date);
    }

    /**
     * 字符串时间比较
     * 
     * @param dateOne
     * @param dateTwo
     * @return 0：两时间相等，负数：dateOne小于dateTwo，正数：dateOne大于dateTwo
     */
    public static Integer dateCompare(String dateOne, String dateTwo) {
        if (isEmpty(dateOne) || isEmpty(dateTwo)) {
            return null;
        }

        try {
            Date date1 = DateUtils.parseDate(dateOne, DATE_PATTERN);
            Date date2 = DateUtils.parseDate(dateTwo, DATE_PATTERN);
            return date1.compareTo(date2);
        } catch (ParseException e) {
            logger.error("Date ParseException:", e);
        }
        return null;
    }

    public static void main(String[] args) {
        System.out.println(DateFormatUtils.strToDate("2018-11-19 1:00:00").getTime());
        System.out.println(DateFormatUtils.strToDate("2018-11-19 2:00:00").getTime());
        System.out.println(DateFormatUtils.strToDate("2018-11-19 3:00:00").getTime());
    }
}
