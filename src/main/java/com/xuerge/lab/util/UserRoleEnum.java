package com.xuerge.lab.util;

/**
 * @description: 用户角色
 * @author: Jeff Zhang
 * @create: 2018-08-01 15:45
 **/
public enum UserRoleEnum {
    DEV("DEV"),
    QA("QA"),
    ADMIN("ADMIN"),
    //运维
    SA("SA"),
    DEV_SA("DEV_SA");

    private String value;

    UserRoleEnum(String roleStr) {
        this.value = roleStr;
    }

    public String getValue() { return value; }

}
