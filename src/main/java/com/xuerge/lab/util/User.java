package com.xuerge.lab.util;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serializable;

/**
 * @description: 用户
 * @author: Jeff Zhang
 * @create: 2018-07-30 20:43
 **/
@Data
@AllArgsConstructor
public class User{

    private String email;
    private String workWechatAvatar;
}

