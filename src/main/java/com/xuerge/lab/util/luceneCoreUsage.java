package com.xuerge.lab.util;

import com.google.common.collect.Maps;
import org.apache.lucene.util.RamUsageEstimator;

import java.util.Map;

public class luceneCoreUsage {
    public static void main(String[] args) {
        String img = "http://p.qlogo.cn/bizmail/DvpbqfIdl35fu2UkEWMHicySIUpGl0iaAic8zqF1rvkPQyVqibUzIlAfiaQ/0";
        User u = new User("shiyi.xu@56qq.com", "http://p.qlogo.cn/bizmail/DvpbqfIdl35fu2UkEWMHicySIUpGl0iaAic8zqF1rvkPQyVqibUzIlAfiaQ/0");
        Map<String, String> data = Maps.newHashMap();
        for (int i = 0; i < 500; i++) {
            data.put("shiyi.xu@56qq.com" + i, img+""+i);
        }

        System.out.println(RamUsageEstimator.sizeOf(data));
        System.out.println(RamUsageEstimator.shallowSizeOf(data));
        System.out.println(RamUsageEstimator.humanSizeOf(data));

        System.out.println(RamUsageEstimator.sizeOf(u));
        System.out.println(RamUsageEstimator.shallowSizeOf(u));
        System.out.println(RamUsageEstimator.humanSizeOf(u));
    }
}
