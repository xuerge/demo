package com.xuerge.lab.util;

import com.google.common.io.Files;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.List;

/**
 * @author shiyi.xu
 * @create 17/7/31.
 */
public class FileUtil {
    private String readFile(String classPath) {
        StringBuilder fileBuff = new StringBuilder();
        Resource script = new ClassPathResource(classPath);
        List<String> lines = null;
        try {
            lines = Files.readLines(script.getFile(), Charset.forName("UTF-8"));
        } catch (IOException e) {

        }
        for (String lin : lines) {
            fileBuff.append(lin + "\n");
        }
        return fileBuff.toString();
    }
}
