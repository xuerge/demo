package com.xuerge.lab.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

/**
 * @author shiyi.xu
 * @create 17/10/9.
 */
@Component
public class TestDao {
    @Autowired
    JdbcTemplate jdbcTemplate;

    public Integer testDaoConnection(){
        return jdbcTemplate.queryForObject("select count(id) FROM app_info;", Integer.class);
    }
}
