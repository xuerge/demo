package com.xuerge.lab.redis;

import com.alibaba.fastjson.JSON;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.util.Assert;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.io.IOException;
import java.util.Iterator;
import java.util.Set;

public class RedisClient {
    @Autowired
    JedisPoolConfig jedisPoolConfig;
    private JedisPool jedisPool;
    @Value("${redis.host}")
    private String host;
    @Value("${redis.port}")
    private int port;
    @Value("${redis.timeout:1000}")
    private int timeout;
    @Value("${redis.password}")
    private String password;

    public RedisClient() {
    }

    public RedisClient(String host,int port,int timeout,String password,JedisPoolConfig jedisPoolConfig) {
        this.host = host;
        this.port = port;
        this.timeout = timeout;
        this.password = password;
        this.jedisPoolConfig = jedisPoolConfig;
        jedisPool = new JedisPool(jedisPoolConfig, host, port, timeout,password);
    }

    @PostConstruct
    public void init() {
        jedisPool = new JedisPool(jedisPoolConfig, host, port, timeout,password);
//        jedisPool = new JedisPool(jedisPoolConfig, host, port, timeout);
    }

    @PreDestroy
    public void destroy() {
        jedisPool.destroy();
    }

    public <V> void set(String key, V value, int expireTime) throws IOException {
        Assert.notNull(key);
        Assert.notNull(value);
        Assert.isTrue(expireTime > 0, "expire second less than zero");

        try (Jedis jedis = jedisPool.getResource()) {
            String serialized = JSON.toJSONString(value);
            jedis.setex(key, expireTime, serialized);
        }
    }

    public <V> V get(String key, Class<V> clazz) throws IOException {
        Assert.notNull(key);
        Assert.notNull(clazz);
        try (Jedis jedis = jedisPool.getResource()) {
            String value = jedis.get(key);
            if (null == value) {
                return null;
            }
            return JSON.parseObject(value, clazz);
        }
    }

    public Set<String> keys(String key) throws IOException {
        Assert.notNull(key);
        Jedis jedis = null;
        try {
            jedis = jedisPool.getResource();
            Set<String> result =  jedis.keys(key);
            return result;
        } finally {
            if (jedis != null) { // not in transaction
                jedis.close();
            }
        }
    }

    public long inc(String key) throws IOException {
        Assert.notNull(key);
        try (Jedis jedis = jedisPool.getResource()) {
            return jedis.incr(key);
        }
    }

    public long hincrBy(String key,String field,long value) throws IOException {
        Assert.notNull(key);
        try (Jedis jedis = jedisPool.getResource()) {
            return jedis.hincrBy(key,field,value);
        }
    }

    public void delete(String key) {
        Assert.notNull(key);
        try (Jedis jedis = jedisPool.getResource()) {
            jedis.del(key);
        }
    }

    public String ping() {
        try (Jedis jedis = jedisPool.getResource()) {
            return jedis.ping();
        }
    }

    public String info() {
        try (Jedis jedis = jedisPool.getResource()) {
            return jedis.info();
        }
    }

    public Object eval(String script, int keyCount, String... params) {
        try (Jedis jedis = jedisPool.getResource()) {
            return jedis.eval(script,keyCount,params);
        }
    }

    public Object eval(String script) {
        try (Jedis jedis = jedisPool.getResource()) {
            return jedis.eval(script);
        }
    }

    public void batchDel(String pattern){
        try (Jedis jedis = jedisPool.getResource()) {
            Set<String> set = jedis.keys(pattern);
            Iterator<String> it = set.iterator();
            while(it.hasNext()){
                String keyStr = it.next();
                jedis.del(keyStr);
            }
        }
    }

    public Jedis getJedis(){
        return jedisPool.getResource();
    }
}
