package com.xuerge.lab.redis;

import org.springframework.beans.factory.annotation.Autowired;

import java.util.Set;

/**
 *
 */
public class TimerService {
    @Autowired
    RedisClient redisClient;

    private static final String key = "solvay:timer_tasks";

    public void putTask(TimerTask task){
        redisClient.getJedis().zadd(key,task.getExecuteTime(),task.serialize());
    }

    public TimerTask getTask(){
        Set<String> taskSet = redisClient.getJedis().zrange(key,0,0);
        if(!taskSet.isEmpty()){
            return null;
        }
        String value = taskSet.iterator().next();
        return TimerTask.deserialize(value);
    }
}
