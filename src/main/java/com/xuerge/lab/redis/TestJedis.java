package com.xuerge.lab.redis;

import org.junit.Before;
import org.junit.Test;
import redis.clients.jedis.JedisPoolConfig;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.FutureTask;

/**
 * @author shiyi.xu
 * @create 17/5/31.
 */
public class TestJedis {
    private String host;
    private int port;
    private int timeout;
    private String password;
    private JedisPoolConfig jedisPoolConfig;
    RedisClient jedisHelper;
    @Before
    public void init(){
//        host = "127.0.0.1";
//        port = 6379;
//        timeout = 30;
//        password = "";
        jedisPoolConfig = new JedisPoolConfig();
//        jedisHelper = new JedisHelper(host,port,timeout,jedisPoolConfig);
        host = "10.6.1.46";
        port = 6443;
        timeout = 100000;
        password = "Dmp^^123";
        jedisPoolConfig = new JedisPoolConfig();
        jedisHelper = new RedisClient(host,port,timeout,password,jedisPoolConfig);
    }
     @Test
    public void test(){
         System.out.println(jedisHelper.ping());
    }


    @Test
    public void testExpiredListener() throws InterruptedException {
        CountDownLatch wait = new CountDownLatch(1);

        FutureTask futureTask = new FutureTask(()->{
            jedisHelper.getJedis().subscribe(new KeyExpiredListener(wait),"__keyevent@0__:expired");
            return null;
        });
        ExecutorService executorService = Executors.newFixedThreadPool(1);
        executorService.submit(futureTask);

        wait.await();
    }

    @Test
    public void eavl() throws ParseException, IOException {
        String script = "local bean_id = KEYS[1]\n" +
                "local expire_second = 10\n" +
                "local allow_times = 120\n" +
                "\n" +
                "-- 记录KEY\n" +
                "local key = 'ErrorCount:' .. bean_id\n" +
                "\n" +
                "local key_exist = redis.call('EXISTS',key)\n" +
                "if key_exist == 1 then\n" +
                "    local count = redis.call('INCR',key)\n" +
                "    if count > allow_times then\n" +
                "        redis.call('DEL',key)\n" +
                "    end\n" +
                "    return count;\n" +
                "else\n" +
                "    redis.call('INCR',key)\n" +
                "    redis.call('EXPIRE',key,expire_second);\n" +
                "    return 1\n" +
                "end\n";
         System.out.println(jedisHelper.eval(script,1,"beanid"));
    }

    public static String getTime(String date) throws ParseException {
        DateFormat formater = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return String.valueOf(formater.parse(date).getTime());
    }

    public RedisClient getClient(){
        return jedisHelper;
    }
}
