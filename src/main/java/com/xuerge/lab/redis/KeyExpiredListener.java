package com.xuerge.lab.redis;

import redis.clients.jedis.JedisPubSub;

import java.util.concurrent.CountDownLatch;

/**
 *
 */
public class KeyExpiredListener extends JedisPubSub {
    CountDownLatch wait;
    public KeyExpiredListener(CountDownLatch wait) {
        this.wait = wait;
    }

    @Override
    public void onMessage(String channel, String message) {
        System.out.println("channel:"+channel);
        System.out.println("message:"+message);
        wait.countDown();
    }
}
