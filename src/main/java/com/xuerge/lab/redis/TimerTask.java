package com.xuerge.lab.redis;

import lombok.Data;

import java.io.Serializable;

/**
 *
 */
@Data
public class TimerTask implements Serializable {
    private String id;
    private long executeTime;
    private int repeatTime = 3;

    public TimerTask(String id, long executeTime) {
        this.id = id;
        this.executeTime = executeTime;
    }

    public TimerTask(String id, long executeTime, int repeatTime) {
        this.id = id;
        this.executeTime = executeTime;
        this.repeatTime = repeatTime;
    }

    public static TimerTask deserialize(String value) {
        String[] values = value.split("_");
        return new TimerTask(values[0], Long.parseLong(values[1]), Integer.parseInt(values[2]));
    }

    public String serialize() {
        return this.getId() + "_" + this.getExecuteTime() + "_" + this.getRepeatTime();
    }
}
