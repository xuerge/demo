package com.xuerge.lab.api;

import com.codahale.metrics.Meter;
import com.xuerge.lab.dao.TestDao;
import com.xuerge.lab.domain.Class;
import com.xuerge.lab.domain.Student;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author shiyi.xu
 * @create 17/8/3.
 */
@RestController
@EnableAutoConfiguration
public class Example {
    Logger logger = LoggerFactory.getLogger(Example.class);

    //    @Autowired
//    Student student;
    @Autowired
    private Class clazz;
    @Autowired
    TestDao testDao;
    @Autowired
    Meter requestMeter;

    @RequestMapping("/")
    Student home() {
        Student s = new Student();
        s.setName("jack");
        logger.info(s.toString());
        //logger.info(String.valueOf(testDao.testDaoConnection()));
        requestMeter.mark();
        return s;
    }


}
