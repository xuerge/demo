package com.xuerge.lab.config;

import com.codahale.metrics.MetricRegistry;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import javax.management.MBeanServer;

/**
 * @author shiyi.xu
 * @create 17/10/9.
 */
//@Configuration
//@ComponentScan({"com.keevol.springboot.metrics.lifecycle", "com.keevol.springboot.metrics.aop"})
//@AutoConfigureAfter(AopAutoConfiguration.class)
public class DropwizardMetricsMBeansAutoConfiguration {
    @Value("${metrics.mbeans.domain.name:com.keevol.metrics}")
    String metricsMBeansDomainName;
    @Autowired
    MBeanServer mbeanServer;
    @Autowired
    MetricRegistry metricRegistry;


}
