package com.xuerge.lab.config;

import com.codahale.metrics.Meter;
import com.codahale.metrics.MetricRegistry;
import com.xuerge.lab.domain.Student;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author shiyi.xu
 * @create 17/9/3.
 */
@Configuration
public class DbConfig {



    @Bean(name = "studentA")
    public Student getStudentA() {
        Student st = new Student();
        st.setName("sss");
        return st;
    }

    @Bean
    public Meter requestMeter(MetricRegistry metrics) {
        return metrics.meter("request");
    }



}
