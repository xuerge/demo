package com.xuerge.lab.proxy;

/**
 * 实际对象
 *
 * @author shiyi.xu
 * @create 18/7/6.
 */
public class RealSubject implements Subject {
    @Override
    public void greeting(String name) {
        System.out.println("hello " + name);
    }
}
