package com.xuerge.lab.proxy;

/**
 * 需要被动态代理的接口
 *
 * @author shiyi.xu
 * @create 18/7/6.
 */
public interface Subject {
    void greeting(String name);
}
