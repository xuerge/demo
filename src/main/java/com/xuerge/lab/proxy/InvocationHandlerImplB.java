package com.xuerge.lab.proxy;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

/**
 * @author shiyi.xu
 * @create 18/7/25.
 */
public class InvocationHandlerImplB implements InvocationHandler {
    /**
     * 需要代理的真实对象
     */
    private Object subject;

    InvocationHandlerImplB(Object subject) {
        this.subject = subject;
    }

    /**
     * 该方法负责处理动态代理类上面的所有方法调用
     * 调用处理器根据这三个参数进行与处理或者分派代委托类实例上反射执行
     *
     * @param proxy  代理类实例
     * @param method 被代理的方法对象
     * @param args   调用参数
     * @return
     * @throws Throwable
     */
    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        System.out.println("InvocationHandlerImplB：before " + method.getName());
        Object result = method.invoke(subject, args);
        System.out.println("InvocationHandlerImplB：after " + method.getName());
        return result;
    }
}
