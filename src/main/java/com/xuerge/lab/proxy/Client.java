package com.xuerge.lab.proxy;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Proxy;

/**
 * @author shiyi.xu
 * @create 18/7/6.
 */
public class Client {
    public static void main(String[] args) {
        // 代理的真实对象
        Subject realSubject = new RealSubject();
        /**
         * InvocationHandlerImp实现的InvocationHandler接口，并能实现方法调用从代理类到委托类的分派转发
         * 要调用哪个真实对象，就将哪个真实对象传进去，最好通过真实对象来调用其方法
         */
        InvocationHandler proxyA = new InvocationHandlerImplA(realSubject);
        ClassLoader classLoader = realSubject.getClass().getClassLoader();
        Class[] interfaces = realSubject.getClass().getInterfaces();

        Subject subject = (Subject) Proxy.newProxyInstance(classLoader,interfaces,proxyA);

        InvocationHandler proxyB = new InvocationHandlerImplB(subject);
        subject = (Subject) Proxy.newProxyInstance(classLoader,interfaces,proxyB);

        System.out.println("动态代理对象的类型："+subject.getClass().getName());
        subject.greeting("tom");
    }
}
