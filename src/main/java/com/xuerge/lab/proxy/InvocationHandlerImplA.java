package com.xuerge.lab.proxy;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

/**
 * @author shiyi.xu
 * @create 18/7/6.
 */
public class InvocationHandlerImplA implements InvocationHandler {
    /**
     * 需要代理的真实对象
     */
    private Object subject;

    InvocationHandlerImplA(Object subject) {
        this.subject = subject;
    }

    /**
     * 该方法负责处理动态代理类上面的所有方法调用
     * 调用处理器根据这三个参数进行与处理或者分派代委托类实例上反射执行
     *
     * @param proxy  代理类实例
     * @param method 被代理的方法对象
     * @param args   调用参数
     * @return
     * @throws Throwable
     */
    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        System.out.println("InvocationHandlerImplA：before " + method.getName());
        Object result = method.invoke(subject, args);
        System.out.println("InvocationHandlerImplA：after " + method.getName());
        return result;
    }

}
