package com.xuerge.lab.cache.store;

/**
 *
 */
public interface ValueHolder<V> {
    V value();
}
