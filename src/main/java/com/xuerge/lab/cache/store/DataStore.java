package com.xuerge.lab.cache.store;

/**
 *
 */
public interface DataStore<K,V> {
    ValueHolder<V> get(K key);
    void put(K key,V value);
    ValueHolder<V> remove(K key);
    void clear();
}
