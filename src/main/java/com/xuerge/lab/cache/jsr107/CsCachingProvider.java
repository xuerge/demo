package com.xuerge.lab.cache.jsr107;

import javax.cache.CacheManager;
import javax.cache.configuration.OptionalFeature;
import javax.cache.spi.CachingProvider;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Map;
import java.util.Properties;
import java.util.WeakHashMap;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/**
 *
 */
public class CsCachingProvider implements CachingProvider{
    private static final String DEFAULT_URI_STRING = "urn:X-cscache:jsr107-default-config";
    private static final URI URI_DEFAULT;
    private final Map<ClassLoader, ConcurrentMap<URI, CacheManager>> cacheManagers = new WeakHashMap<ClassLoader,ConcurrentMap<URI,CacheManager>>();

    static{
        try{
            URI_DEFAULT = new URI(DEFAULT_URI_STRING);
        } catch (URISyntaxException e) {
            e.printStackTrace();
            throw new javax.cache.CacheException(e);
        }
    }



    @Override
    public ClassLoader getDefaultClassLoader() {
        return null;
    }

    @Override
    public URI getDefaultURI() {
        return null;
    }

    @Override
    public Properties getDefaultProperties() {
        return null;
    }

    @Override
    public void close() {

    }

    @Override
    public void close(ClassLoader classLoader) {

    }

    @Override
    public void close(URI uri, ClassLoader classLoader) {

    }

    @Override
    public CacheManager getCacheManager() {
        return getCacheManager(getDefaultURI(),getDefaultClassLoader(),null);
    }

    @Override
    public CacheManager getCacheManager(URI uri, ClassLoader classLoader) {
        return getCacheManager(uri, classLoader,getDefaultProperties());
    }

    @Override
    public CacheManager getCacheManager(URI uri, ClassLoader classLoader, Properties properties) {
        uri = uri == null ? getDefaultURI() : uri;
        classLoader = classLoader == null ? getDefaultClassLoader() : classLoader;
        properties = properties == null ? getDefaultProperties() : properties;

        ConcurrentMap<URI,CacheManager> cacheManagersByURI = cacheManagers.get(classLoader);
        if (cacheManagersByURI == null) {
            cacheManagersByURI = new ConcurrentHashMap<URI, CacheManager>();
        }

        CacheManager cacheManager = cacheManagersByURI.get(uri);

        if (cacheManager == null) {
            cacheManager = new CsCachingManager(this, properties, classLoader, uri);

            cacheManagersByURI.put(uri, cacheManager);
        }

        if (!cacheManagers.containsKey(classLoader)) {
            cacheManagers.put(classLoader, cacheManagersByURI);
        }
        return cacheManager;
    }

    @Override
    public boolean isSupported(OptionalFeature optionalFeature) {
        return false;
    }


}
