package com.xuerge.lab.fsm;


import com.alibaba.fastjson.JSON;

/**
 * @author shiyi.xu
 * @create 18/4/19.
 */
public enum States {
    READY("待测试"),
    REJECT("拒绝接收"),
    TESTING("测试中"),
    PASS("通过测试"),
    FAIL("失败");

    private String name;
    States(String name) {
        this.name = name;
    }

    public static void main(String[] args) {
        System.out.println(JSON.toJSONString(States.READY));
    }
}
