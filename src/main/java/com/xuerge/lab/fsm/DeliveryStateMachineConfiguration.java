package com.xuerge.lab.fsm;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.statemachine.config.EnableStateMachine;
import org.springframework.statemachine.config.StateMachineConfigurerAdapter;
import org.springframework.statemachine.config.builders.StateMachineStateConfigurer;
import org.springframework.statemachine.config.builders.StateMachineTransitionConfigurer;

import java.util.EnumSet;

/**
 * 提测状态机创建，配置
 *
 * @author shiyi.xu
 * @create 18/4/20.
 */
@Configuration
@EnableStateMachine(name = "deliveryStateMachine")
@Slf4j
public class DeliveryStateMachineConfiguration extends StateMachineConfigurerAdapter<States, Events> {
    @Autowired
    private Actions actions;

    @Override
    public void configure(StateMachineStateConfigurer<States, Events> config) throws Exception {
        config.withStates()
                .initial(States.READY)
                .states(EnumSet.allOf(States.class));
    }


    @Override
    public void configure(StateMachineTransitionConfigurer<States, Events> transitions) throws Exception {
        transitions
                .withExternal()
                .source(States.READY).target(States.TESTING)
                .event(Events.accept).action(actions.accept())
                .and()
                .withExternal()
                .source(States.TESTING).target(States.REJECT)
                .event(Events.reject).action(actions.reject())
                .and()
                .withExternal()
                .source(States.TESTING).target(States.FAIL)
                .event(Events.test_failed).action(actions.fail())
                .and()
                .withExternal()
                .source(States.TESTING).target(States.PASS)
                .event(Events.test_success).action(actions.pass())
                .and()
                .withExternal()
                .source(States.FAIL).target(States.READY).action(actions.ready())
                .event(Events.resubmit);
    }



}
