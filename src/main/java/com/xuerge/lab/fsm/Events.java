package com.xuerge.lab.fsm;

/**
 * @author shiyi.xu
 * @create 18/4/19.
 */
public enum Events {
    reject("拒绝接收"),
    accept("接收"),
    test_failed("测试不通过"),
    test_success("测试通过"),
    resubmit("重新提交测试");

    private String text;

    Events(String name) {
        this.text = name;
    }

    public String getText() {
        return text;
    }
}
