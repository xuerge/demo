package com.xuerge.lab.fsm;

import lombok.extern.slf4j.Slf4j;
import org.springframework.statemachine.action.Action;
import org.springframework.stereotype.Component;

/**
 * @author shiyi.xu
 * @create 18/4/20.
 */
@Component
@Slf4j
public class Actions {


    public Action<States, Events> accept() {
        return ctx -> {
            System.out.println("accept");
        };
    }

    public Action<States,Events> reject() {
        return ctx -> {
            System.out.println("reject");
        };
    }

    public Action<States,Events> fail() {
        return ctx -> {
            System.out.println("faile");
        };
    }

    public Action<States,Events> pass() {
        return ctx -> {
            System.out.println("pass");
        };
    }

    public Action<States,Events> ready() {
        return ctx -> {
            System.out.println("ready");
        };
    }
}
