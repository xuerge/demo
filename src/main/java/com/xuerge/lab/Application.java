package com.xuerge.lab;


import com.codahale.metrics.ConsoleReporter;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;

import java.util.concurrent.TimeUnit;

/**
 * @author shiyi.xu
 * @create 17/8/3.
 */
@SpringBootApplication
//@Configuration
//@EnableAutoConfiguration
//@ComponentScan
//@ImportResource("/applicationContext.xml")
public class Application {
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);

//        ApplicationContext ctx = SpringApplication.run(Application.class, args);
//
//        // 启动Reporter
//        ConsoleReporter reporter = ctx.getBean(ConsoleReporter.class);
//        reporter.start(30, TimeUnit.SECONDS);

    }
}
