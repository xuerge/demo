package com.xuerge.lab.thread;

/**
 * @author shiyi.xu
 * @create 17/6/14.
 */
public class ThreadLocalTest {
    //创建一个Integer型的线程本地变量
    public static final ThreadLocal<Integer> local = new ThreadLocal<Integer>() {
        @Override
        protected Integer initialValue() {
            return 0;
        }
    };


}
