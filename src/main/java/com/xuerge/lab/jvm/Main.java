package com.xuerge.lab.jvm;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * @author shiyi.xu
 * @create 18/7/22.
 */
public class Main {
    public static List staticList = new ArrayList<>();

    public static void main(String[] args) throws Exception {
        List global = new ArrayList<>();
//        int i = 0;
        System.out.println("请输入：1:new Object");
        Scanner sc = new Scanner(System.in);
        while (sc.hasNext()) {
            String cmd = sc.next();
            if ("1".equals(cmd)) {
                print("execute new Object:");
                newObject(global);
            }else if ("2".equals(cmd)) {
                print("execute new Object:");
                newObject(null);
            }
            print("done");
        }
    }

    public static void newObject(List global) throws Exception {
        ObjectA obj = new ObjectA();
        if(null != global){
            global.add(obj);
        }

    }

    public static void print(String msg) {
        System.out.println(msg);
    }


}

class ObjectA {
    private File file;
    List storage = new ArrayList();

    public ObjectA() throws Exception {
        FileInputStream reader = new FileInputStream((new File("/Users/eben/lab/lab/src/main/java/com/xuerge/lab/jvm/1.png")));
        byte[] buffer = new byte[2048];
        //按照行的形式取出数据。取出的每一个行数据不包含回车符。
        while (reader.read(buffer) != -1){
            storage.add(buffer.clone());
        }
        reader.close();
    }
}


