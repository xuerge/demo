package com.xuerge.lab.domain;

import com.alibaba.fastjson.annotation.JSONField;

/**
 * @author shiyi.xu
 * @create 17/4/30.
 */
public class Student {
    private String name;

    @JSONField(name = "aa")
    private String age="";

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Student(String name) {
        this.name = name;
    }

    public Student() {
    }

    @Override
    public String toString() {
        return "Student{" +
            "name='" + name + '\'' +
            '}';
    }
}
