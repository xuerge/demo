package com.xuerge.lab.security;

import org.apache.commons.codec.digest.DigestUtils;

/**
 * @author shiyi.xu
 * @create 17/5/10.
 */
public class MD {
    public static byte[] encodeMD5(String data){
        return DigestUtils.md5(data);
    }

    public static String decodeMD5Hex(String data){
        return DigestUtils.md5Hex(data);
    }

}
