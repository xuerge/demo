package com.xuerge.lab.security;

import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.HashMap;
import java.util.Map;

import javax.crypto.Cipher;

/**
 * @author shiyi.xu
 * @create 17/5/14.
 */
public class RSACoder {
    // 非对称加密秘钥算法
    public static final String KEY_ALGORITHM = "RSA";
    // 公钥
    private static final String PUBLIC_KEY = "RSAPublicKey";
    // 私钥
    private static final String PRIVATE_KEY = "RSAPrivateKey";

    /**
     * RSA 密钥长度
     * 默认1024，
     * 密钥长度必须是64的倍数，
     * 范围在512~65536之间
     */
    private static final int KEY_SIZE = 512;

    /**
     * 是要解密
     * @param data 带解密数据
     * @param privateKey 私钥
     * @return byte[] 解密后的数据
     * @throws Exception
     */
    public static byte[] decryptByPrivateKey(byte[] data,byte[] privateKey) throws Exception{
        // 生成私钥
        PrivateKey priKey = RSACoder.getPrivateKey(privateKey);
        // 对数据解密
        Cipher cipher = Cipher.getInstance(KEY_ALGORITHM);
        cipher.init(Cipher.DECRYPT_MODE,priKey);
        return cipher.doFinal(data);
    }

    /**
     * 公钥解密
     * @param data 待解密数据
     * @param publicKey 公钥
     * @return byte[] 解密后的数据
     * @throws Exception
     */
    public static byte[] decryptByPublicKey(byte[] data,byte[] publicKey) throws Exception{
        // 生成公钥
        PublicKey pubKey = RSACoder.getPublicKey(publicKey);
        // 解密数据
        Cipher cipher = Cipher.getInstance(KEY_ALGORITHM);
        cipher.init(Cipher.DECRYPT_MODE,pubKey);
        return cipher.doFinal(data);
    }

    /**
     * 公钥加密
     * @param data 待加密数据
     * @param publicKey 公钥
     * @return byte[] 加密数据
     * @throws Exception
     */
    public static byte[] encryptByPublicKey(byte[] data,byte[] publicKey) throws Exception{
        // 取得公钥
        PublicKey pubKey = RSACoder.getPublicKey(publicKey);
        // 加密数据
        Cipher cipher = Cipher.getInstance(KEY_ALGORITHM);
        cipher.init(Cipher.DECRYPT_MODE,pubKey);
        return cipher.doFinal(data);
    }

    /**
     * 私钥加密
     */
    public static byte[] encryptByPrivateKey(byte[] data,byte[] privateKey) throws Exception{
        PrivateKey priKey = RSACoder.getPrivateKey(privateKey);
        // 加密数据
        Cipher cipher = Cipher.getInstance(KEY_ALGORITHM);
        cipher.init(Cipher.DECRYPT_MODE,priKey);
        return cipher.doFinal(data);
    }

    /**
     * 初始化密钥
     * @return
     * @throws Exception
     */
    public static Map<String,Object> initKey() throws Exception{
        // 实例化密钥对生成器
        KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance(KEY_ALGORITHM);
        // 初始化秘钥对生成器
        keyPairGenerator.initialize(KEY_SIZE);
        // 生成密钥对
        KeyPair keyPair = keyPairGenerator.generateKeyPair();
        RSAPublicKey publicKey = (RSAPublicKey)keyPair.getPublic();
        RSAPrivateKey privateKey = (RSAPrivateKey)keyPair.getPrivate();
        Map<String,Object> keyMap = new HashMap<>();
        keyMap.put(PUBLIC_KEY,publicKey);
        keyMap.put(PRIVATE_KEY,privateKey);
        return keyMap;

    }



    /**
     * 生成公钥
     */
    public static PublicKey getPublicKey(byte[] publicKey) throws NoSuchAlgorithmException, InvalidKeySpecException {
        // 取得公钥
        X509EncodedKeySpec x509EncodedKeySpec = new X509EncodedKeySpec(publicKey);
        KeyFactory keyFactory = KeyFactory.getInstance(KEY_ALGORITHM);
        // 生成公钥
        return keyFactory.generatePublic(x509EncodedKeySpec);
    }

    /**
     * 生成私钥
     */
    public static PrivateKey getPrivateKey(byte[] privateKey) throws NoSuchAlgorithmException, InvalidKeySpecException {
        // 取得私钥
        PKCS8EncodedKeySpec pkcs8EncodedKeySpec = new PKCS8EncodedKeySpec(privateKey);
        KeyFactory keyFactory = KeyFactory.getInstance(KEY_ALGORITHM);
        // 生成私钥
        return keyFactory.generatePrivate(pkcs8EncodedKeySpec);
    }

}
