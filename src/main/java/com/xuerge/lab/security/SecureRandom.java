package com.xuerge.lab.security;

import org.apache.commons.codec.binary.Hex;

/**
 * @author shiyi.xu
 * @create 17/5/14.
 */
public class SecureRandom {
    public String getSecureRandom(){
        java.security.SecureRandom random = new java.security.SecureRandom();
        byte seed[] = random.generateSeed(1);
        return Hex.encodeHexString(seed);
    }
}
