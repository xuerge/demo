package com.xuerge.lab.log;

import lombok.extern.slf4j.Slf4j;
import org.slf4j.MDC;
import org.springframework.util.StringUtils;

import javax.servlet.*;
import java.io.IOException;


/**
 * @author shiyi.xu
 * @create 18/9/20.
 */
@Slf4j
public class RequestFilter implements Filter {
    private final String REQUEST_ID_KEY = "request_id";

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        String requestId = String.valueOf(CurrentTimeId.nextId());
        registerRequestId(requestId);
        try {
            chain.doFilter(request, response);
        }finally {
            // ensure MDC was clear
            MDC.clear();
        }
    }

    private boolean registerRequestId(String requestId) {
        boolean isRegistered = false;
        if (!StringUtils.isEmpty(requestId)) {
            MDC.put(REQUEST_ID_KEY, requestId);
            isRegistered = true;
        }
        return isRegistered;
    }




    @Override
    public void destroy() {

    }
}
