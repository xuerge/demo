package com.xuerge.lab.jmx.standard;

/**
 * @author shiyi.xu
 * @create 17/11/20.
 */
public interface HelloMBean {
    public String getName();
    public void setName(String name);
    public void printHello();
    public void printHello(String name);
}
