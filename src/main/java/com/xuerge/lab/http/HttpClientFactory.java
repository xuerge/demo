package com.xuerge.lab.http;

import org.apache.http.client.HttpRequestRetryHandler;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.config.Registry;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.socket.PlainConnectionSocketFactory;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;

import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

/**
 * 系统中HttpClientManager，支持http,https
 * 
 * @author shiyi.xu
 * @create 17/3/21.
 */
public class HttpClientFactory {
    /** 最大连接数 */
    public final static int MAX_TOTAL = 60;
    /** 每个路由最大连接数 */
    public final static int MAX_PER_ROUTE = 30;
    /** 连接超时时间 */
    public final static int CONNECTION_TIMEOUT = 2000;
    /** 读取超时时间 */
    public final static int SOCKET_TIMEOUT = 3000;
    /** 连接池获取连接 超时时间 */
    public final static int CONNECTION_REQUEST_TIMEOUT = 2000;
    /** http重试 */
    HttpRequestRetryHandler retryHandler;

    private static CloseableHttpClient instance = null;
    public static CloseableHttpClient getSingleInstance(){
        if (instance == null) {
            synchronized (CloseableHttpClient.class) {
                if (instance == null) {
                    instance = createHttpClient();
                }
            }
        }
        return instance;
    }

    private static CloseableHttpClient createHttpClient(){
        // 设置超时时间
        RequestConfig defaultRequestConfig =
            RequestConfig.custom().setConnectTimeout(CONNECTION_TIMEOUT)
                .setSocketTimeout(SOCKET_TIMEOUT)
                .setConnectionRequestTimeout(CONNECTION_REQUEST_TIMEOUT)
                .build();
        /** 连接池  */
        PoolingHttpClientConnectionManager connectionManager = newConnectionManager();

        instance =
            HttpClients.custom().setConnectionManager(connectionManager)
                .setDefaultRequestConfig(defaultRequestConfig)
                .setRetryHandler(null).build();
        return instance;
    }

    /**
     * 创建连接池
     */
    private static PoolingHttpClientConnectionManager newConnectionManager() {
        // 获取TLS安全协议上下文
        SSLContext context = null;
        try {
            context = SSLContext.getInstance("TLS");
            /** 自定义 TrustManager 重写验证方法，取消检测SSL：  */
            X509TrustManager xtm = newTrustManager();
            context.init(null, new TrustManager[] {xtm}, null);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (KeyManagementException e) {
            e.printStackTrace();
        }

        SSLConnectionSocketFactory scsf =
                new SSLConnectionSocketFactory(context, NoopHostnameVerifier.INSTANCE);

        Registry<ConnectionSocketFactory> sfr =
                RegistryBuilder.<ConnectionSocketFactory>create()
                        .register("http", PlainConnectionSocketFactory.INSTANCE)
                        .register("https", scsf).build();


        PoolingHttpClientConnectionManager connManager =
                new PoolingHttpClientConnectionManager(sfr);
        connManager.setDefaultMaxPerRoute(MAX_PER_ROUTE);
        connManager.setMaxTotal(MAX_TOTAL);
        return connManager;
    }

    /** 请求重试 */

    /** 重写验证方法，取消检测SSL */
    private static X509TrustManager newTrustManager(){
        return new X509TrustManager() {
            @Override
            public void checkClientTrusted(X509Certificate[] arg0, String arg1)
                throws CertificateException {}

            @Override
            public void checkServerTrusted(X509Certificate[] arg0, String arg1)
                throws CertificateException {}

            @Override
            public X509Certificate[] getAcceptedIssuers() {
                return new X509Certificate[] {};
            }
        };
    }




}
