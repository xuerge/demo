package com.xuerge.lab.http;

import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.util.List;

/**
 * @author shiyi.xu
 * @create 17/4/14.
 */
public class HttpExecutor {

    private static CloseableHttpClient client = HttpClientFactory.getSingleInstance();


    public static String execute(HttpUriRequest request) throws IOException {
        String responseContent = "";
        try (CloseableHttpResponse response = client.execute(request)) {
            HttpEntity entity = response.getEntity();
            responseContent = EntityUtils.toString(entity);
            EntityUtils.consume(entity);
            return responseContent;
        }
    }

    public static String get(String uri) throws IOException {
        HttpGet get = new HttpGet(uri);
        return execute(get);
    }

    public static String post(String uri, List<NameValuePair> reqParam) throws IOException {
        HttpPost post = new HttpPost(uri);
        post.setEntity(new UrlEncodedFormEntity(reqParam));
        return execute(post);
    }
}
