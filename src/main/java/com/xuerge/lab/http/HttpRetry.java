package com.xuerge.lab.http;

import org.apache.http.HttpEntityEnclosingRequest;
import org.apache.http.HttpRequest;
import org.apache.http.NoHttpResponseException;
import org.apache.http.client.HttpRequestRetryHandler;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.protocol.HttpContext;

import java.io.IOException;
import java.net.UnknownHostException;

/**
 * @author shiyi.xu
 * @create 17/4/15.
 */
public class HttpRetry implements HttpRequestRetryHandler {
    public boolean retryRequest(IOException exception, int executionCount,
                                HttpContext context) {

        if (executionCount >= 2) {
            // 重试1次，第二次失败不重试
            return false;
        }
        if (exception instanceof UnknownHostException
            || exception instanceof ConnectTimeoutException) {
            // 目前调用三方短信服务主要有这两种IO异常，重试
            return true;
        }
        if (exception instanceof UnknownHostException
            || exception instanceof ConnectTimeoutException
            || exception instanceof NoHttpResponseException) {
            // 目前调用三方短信服务主要有这两种IO异常，重试
            return true;
        }

        HttpClientContext clientContext = HttpClientContext.adapt(context);
        HttpRequest request = clientContext.getRequest();
        boolean idempotent = !(request instanceof HttpEntityEnclosingRequest);
        if (idempotent) {
            // Retry if the request is considered idempotent
            return true;
        }
        return false;
    }
}
