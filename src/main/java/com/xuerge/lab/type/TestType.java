package com.xuerge.lab.type;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;

/**
 *
 */
public class TestType {


    public static void main(String[] args) throws NoSuchFieldException {
        Map<String,String> map = new HashMap<>();
        // 如何获取一个变量的Type ? Class实现了Type接口，所以直接class转化为Type
        Type type = map.getClass();
        System.out.println(type);

        map.getClass().getGenericSuperclass();
        if(map.getClass().getGenericSuperclass() instanceof ParameterizedType){
            System.out.println("Type is instanceof ParameterizedType");
        }


    }

}
